require 'net/http'
require 'uri'
require 'json'

module Gitlab
  module Llm
    module Anthropic
      class ClaudeSpecialClient
        ANTHROPIC_URL = 'https://api.anthropic.com/v1/messages'
        DEFAULT_MODEL = 'claude-3-opus-20240229'
        DEFAULT_TEMPERATURE = 0
        DEFAULT_MAX_TOKENS = 4096
        DEFAULT_TIMEOUT = 30 # seconds
        DEFAULT_PROVIDER = 'anthropic'
        DEFAULT_TYPE = 'prompt'
        DEFAULT_SOURCE = 'GitLab EE'
        FILE_PATH = 'FILE_PATH'
        VULN_REPORT_PATH = 'VULN_REPORT_PATH'
        ANTHROPIC_ACCESS_TOKEN = 'ANTHROPIC_ACCESS_TOKEN'

        def send_message
          uri = URI(ANTHROPIC_URL)

          headers = {
            'x-api-key' => access_token,
            'anthropic-version' => '2023-06-01',
            'content-type' => 'application/json'
          }

          response = Net::HTTP.post(uri, request_body.to_json, headers)

          JSON.parse(response.body)['content'][0]['text']
        end

        def access_token
          raise "No access token found" unless ENV[ANTHROPIC_ACCESS_TOKEN]
          ENV[ANTHROPIC_ACCESS_TOKEN]
        end

        def request_body
          {
            model: DEFAULT_MODEL,
            max_tokens: DEFAULT_MAX_TOKENS,
            messages: prompt,
            system: system_prompt,
            stop_sequences: ["```", "</updated_version>"]
          }
        end

        def prompt
          [
              {
                "role" => "user",
                "content" => "#{file_name}: ```#{file_contents}``` \n\n Advisory: ```#{vuln_json}```"
              },
              {
                "role" => "assistant",
                "content" => "<updated_version>"
              },
            ]
        end

        def system_prompt
          "Give me the upgraded version of the dependency in #{file_name}, per the advisory, returned in <updated_version> tag."
        end

        def vuln_json
          raise "no vulnerability path configured" unless ENV[VULN_REPORT_PATH]
          File.read(ENV[VULN_REPORT_PATH])
        end

        def file_path_guard
          raise "no file path configured" unless ENV[FILE_PATH]
        end

        def file_contents
          file_path_guard
          path = ENV[FILE_PATH]
          raise "File #{path} does not exist" unless File.exist?(path)
          File.read(path)
        end

        def file_contents_start
          file_contents[0..5]
        end

        def file_name
          file_path_guard
          ENV[FILE_PATH]
        end

      end
    end
  end
end
