# Additional code to use ClaudeSpecialClient
require_relative 'claude_client' # Include the main script if it's in the same directory

# Initialize the ClaudeSpecialClient
client = Gitlab::Llm::Anthropic::ClaudeSpecialClient.new


puts "Sending message to Anthropic API"
# Send the message
begin
  response = client.send_message
  puts(response)
  # puts "Response from Anthropic API: #{response}"
rescue => e
  puts "An error occurred: #{e.message}"
end
