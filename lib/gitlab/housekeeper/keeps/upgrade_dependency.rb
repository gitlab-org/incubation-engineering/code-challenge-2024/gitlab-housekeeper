# frozen_string_literal: true

require 'gitlab/housekeeper/keeps/claude_client'

module Gitlab
  module Housekeeper
    module Keeps
      class UpgradeDependency < Keep
        def initialize(
          upgrade_command: nil, # bundle update haml
          changed_files: ENV.fetch('HOUSEKEEPER_DEPENDENCY_UPGRADE_CHANGED_FILES').split(',') # Gemfile.lock
        )
          @upgrade_command = "mvn versions:update-property -Dproperty=log4j.version -DnewVersion='#{ Gitlab::Llm::Anthropic::ClaudeSpecialClient.new.send_message }'"
          @changed_files = changed_files
        end

        def each_change
          ::Gitlab::Housekeeper::Shell.execute(@upgrade_command)

          change = Change.new
          change.title = "Run #{@upgrade_command}"
          change.identifiers = [self.class.name, @upgrade_command]
          change.description = <<~MARKDOWN
          Automatically upgrade a dependency using:

          ```
          #{@upgrade_command}
          ```
          MARKDOWN


          change.changed_files = @changed_files

          yield(change)
        end
      end
    end
  end
end
